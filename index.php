<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Конвертер</title>
    <link rel="stylesheet" href="/css/styles.css">
  </head>
  <body>
    <h1>Введите текст теста (в формате "xaiken"):</h1>
    <form id="form-converter">
      <div><textarea name="text_of_test" rows="25" cols="150"></textarea></div>
      <div><button type="submit">Конвертировать</button></div>
    </form>
    <div id="message"></div>
    <script src="jquery.min.js"></script>
    <script src="script.js"></script>
  </body>
</html>
