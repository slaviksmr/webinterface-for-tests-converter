<?php

include "XaikenQuestionBank.php";
include "XaikenGiftIO.php";

$tempDirectory = "tmp/";
if (!file_exists($tempDirectory)) {
    if (!mkdir($tempDirectory, 0777, true)) {
        die('Не удалось создать директорию...');
    }
} else {
    foreach (glob($tempDirectory . "*") as $file) {
        if (time() - filectime($file) > 1800) {
            unlink($file);
        }
    }
}
$uniqueInputFile = uniqid() . ".xaiken.txt";
$uniqueOutputFile = uniqid() . ".gift.txt";
$filePathInput = $tempDirectory . $uniqueInputFile;
$filePathOutput = $tempDirectory . $uniqueOutputFile;

if (!empty($_POST['text_of_test'])) {
    $fileopen = fopen($filePathInput, "w+");
    fwrite($fileopen, $_POST['text_of_test']);
    fclose($fileopen);
    $io = new XaikenMultiformatIO();
    $bank = $io->readBank($filePathInput);
    unlink($filePathInput);
    if (!empty($io->log)) {
        foreach ($io->log as $error) {
            print_r($error);
        }
    } else {
        $io->writeBank($filePathOutput, $bank);
        print_r("Файл готов. <a href=" . $filePathOutput . " download>Скачать файл в формате GIFT</a>");
    }
}
